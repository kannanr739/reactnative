import React, {Component} from 'react';
import {Text, Button, View, StyleSheet, ScrollView, Picker, Switch, Modal, Alert} from 'react-native';
import { Card } from 'react-native-elements';
import DatePicker from 'react-native-datepicker';
import * as Animatable from 'react-native-animatable';
import { Notifications} from 'expo';
import * as Permissions from 'expo-permissions';
import * as Calendar from 'expo-calendar';
class Reservation extends Component {
    constructor(props) {
        super(props);
        this.state = {
            guests: 1,
            smoking: false,
            date: '',
        }
    }

    toggleModal() {
        this.setState({showModal: !this.state.showModal})
    }
    static navigationOptions = {
        title: 'Reserve Table'
    }
    handleReservation() {
        const {guests, smoking, date} = this.state;
        Alert.alert(
            'Your Reservation OK?',
            `Number of Guests: ${guests} \nSmoking? ${smoking} \nDate and Time: ${date}`,
            [
                {text: 'Cancel', onPress: () => {console.log('Cancel Reservaton Confirmation'); this.resetForm()}, style: 'cancel'},
                {text: 'Ok', onPress: () => {this.addReservationToCalendar(this.state.date); console.log('Reservation Confirmed'); this.presentLocalNotification(this.state.date); this.resetForm()}}
            ],
            {cancelable: false}
        )
    }

    resetForm() {
        this.setState({
            guests: 1,
            smoking: false,
            date: '',
        })
    }

    async obtainNotificationPermission() {
        let permission = await Permissions.getAsync(Permissions.USER_FACING_NOTIFICATIONS)
        if (permission.status !== 'granted') {
            permission = await Permissions.askAsync(Permissions.USER_FACING_NOTIFICATIONS)
            if (permission.status !== 'granted') {
                Alert.alert('Permission not granted to show notifications')
            }
        }
        return permission;
    }

    async obtainCalendarPermission() {
        let permission = await Permissions.getAsync(Permissions.CALENDAR)
        if (permission.status !== 'granted') {
            permission = await Permissions.askAsync(Permissions.CALENDAR)
            if (permission.status !== 'granted') {
                Alert.alert('Permission not granted to add event to calendar')
            }
        }
        return permission;
    }

    async presentLocalNotification(date) {
        await this.obtainNotificationPermission();
        Notifications.presentLocalNotificationAsync({
            title: 'Your Reservation',
            body: 'Reservation for ' + date + ' requested',
            ios: {
                sound: true,
            },
            android: {
                sound: true,
                vibrate: true,
                color: '#512DA8'
            }
        })
    }

    async addReservationToCalendar(date) {
        await this.obtainCalendarPermission();
        const details = {
            id: 'd2121',
            title: 'Con Fusion Table Reservation',
            startDate: Date.parse(date),
            endDate: Date.parse(date) + 2*3600*1000,
            timeZone: 'Asia/Hong_Kong',
            location: '121, Clear Water Bay Road, Clear Water Bay, Kowloon, Hong Kong'
        }

        Calendar.createEventAsync("3", details)
        .then(result => console.log(result))    
        .catch(result => console.log(result))
            

    }
    render() {
        return (
            <Animatable.View animation="zoomIn" duration={2000} delay={1000}>
                <ScrollView>
                    <View style={styles.formRow}>
                        <Text style={styles.formLabel}> Number of guests: </Text>
                        <Picker 
                            style={styles.formItem}
                            selectedValue={this.state.guests}
                            onValueChange={(itemValue, itemIndex) => this.setState({guests: itemValue})}
                        >
                            <Picker.Item label="1" value={1} />
                            <Picker.Item label="2" value={2} />
                            <Picker.Item label="3" value={3} />
                            <Picker.Item label="4" value={4} />
                            <Picker.Item label="5" value={5} />
                            <Picker.Item label="6" value={6} />
                        </Picker>
                    </View>
                    <View style={styles.formRow}>
                        <Text style={styles.formLabel}> Smoking </Text>
                        <Switch
                            style={styles.formItem}
                            value={this.state.smoking}
                            trackColor={'#512DA8'}
                            onValueChange={(value) => this.setState({smoking : value})}
                        />
                    </View>
                    <View style={styles.formRow}>
                        <Text style={styles.formLabel}> Date and Time </Text>
                        <DatePicker
                            style={{flex: 2, marginRight: 20}}
                            date={this.state.date}
                            format=""
                            mode="datetime"
                            placeholder="Select Date and Time"
                            minDate={new Date()}
                            confirmBtnText='Confirm'
                            cancelBtnText='Cancel'
                            customStyles={{
                                dateIcon: {
                                    position: 'absolute',
                                    top: 4,
                                    left: 0,
                                    marginLeft: 0
                                },
                                dateInput: {
                                    marginLeft: 36
                                }
                            }}
                            onDateChange={(date) => {this.setState({date: date})}}
                        />
                    </View>
                    <View style={styles.formRow}>
                        <Button 
                            title="Reserve"
                            color="#512DA8"
                            onPress={() => this.handleReservation()}
                            accessibilityLabel="Learn more about this purple button"
                        />
                    </View>
                </ScrollView>
            </Animatable.View>
        );
    }
}

const styles = StyleSheet.create({
    formRow: {
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
        flexDirection: 'row',
        margin: 20
    },
    formLabel: {
        fontSize: 18,
        flex: 2
    },
    formItem: {
        flex: 1
    },
    modal: {
        justifyContent: 'center',
        margin: 20
    },
    modalTitle: {
        fontSize: 24,
        fontWeight: 'bold',
        backgroundColor: '#512DA8',
        textAlign: 'center',
        color: 'white',
        marginBottom: 20
    },
    modalText: {
        fontSize: 18,
        margin: 10
    }
})
export default Reservation;