import React, {Component} from 'react';
import {View, StyleSheet, Text, ScrollView, Image} from 'react-native';
import {Icon, Input, CheckBox, Button} from 'react-native-elements';
import * as SecureStore from 'expo-secure-store';
import * as Permissions from 'expo-permissions';
import * as ImagePicker from 'expo-image-picker';
import * as ImageManipulator from 'expo-image-manipulator';
import {Asset} from 'expo-asset';
import {createBottomTabNavigator} from 'react-navigation';
import {baseURL} from '../shared/baseURL';
class LoginTabComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: '',
            remember: false
        }
    }

    componentDidMount() {
        SecureStore.getItemAsync('userinfo')
            .then(userdata => {
                let userinfo = JSON.parse(userdata)
                if (userinfo) {
                    this.setState({...userinfo, remember: true})
                }
            })
    }

    static navigationOptions = {
        title: 'Login',
        tabBarIcon: ({tintColor}) => (
            <Icon 
                name='sign-in'
                type='font-awesome'
                size={24}
                iconStyle={{color: tintColor}}
            />
        )
    };
    handleLogin() {
        console.log(JSON.stringify(this.state));
        if(this.state.remember) {
            SecureStore.setItemAsync('userinfo', JSON.stringify({username: this.state.username, password: this.state.password}))
                .catch(error => {
                    console.log("Couldn't save user info", error);
                })
        }
        else {
            SecureStore.deleteItemAsync('userinfo')
                .catch(error => {
                    console.log("Couldn't delete user info", error);
                })
        }
        
    }
    render() {
        return(
            <View style={styles.container}>
                <Input 
                    placeholder="Username"
                    leftIcon={{type:'font-awesome', name:'user-o'}}
                    onChangeText={(username) => this.setState({username})}
                    value={this.state.username}
                    containerStyle={styles.formInput}
                />
                <Input 
                    placeholder="Password"
                    leftIcon={{type:'font-awesome', name:'key'}}
                    onChangeText={(password) => this.setState({password})}
                    value={this.state.password}
                    containerStyle={styles.formInput}
                />
                <CheckBox 
                    title="Remeber Me"
                    center
                    checked={this.state.remember}
                    onPress={() => this.setState({remember: !this.state.remember})}
                    containerStyle={styles.formCheckBox}
                />
                <View style={styles.formButton}>
                <Button 
                        onPress={() => this.handleLogin()}
                        title="Login"
                        buttonStyle={{backgroundColor: "#512DA8"}}
                        icon={<Icon name='sign-in' size={24} type='font-awesome' color='white' />}
                    />
                </View>
                <View style={styles.formButton}>
                    <Button 
                        clear
                        onPress={() => this.props.navigation.navigate('Register')}
                        title="Register"
                        titleStyle={{color: 'blue'}}
                        icon={<Icon name='user-plus' size={24} type='font-awesome' color='blue' />}
                    />
                </View>
            </View>
        )
    }
}

class RegisterTabComponent extends Component {
    constructor(props){
        super(props);
        this.state = {
            username: '',
            password: '',
            firstname: '',
            lastname: '',
            email: '',
            remember: false,
            imageUrl: baseURL + 'images/logo.png'
        }
    }

    getImageFromCamera = async() => {
        const cameraPermission = await Permissions.askAsync(Permissions.CAMERA);
        const cameraRollPermission = await Permissions.askAsync(Permissions.CAMERA_ROLL);
        if (cameraPermission.status === 'granted' && cameraRollPermission.status === 'granted') {
            let capturedImage = await ImagePicker.launchCameraAsync({
                allowsEditing: true,
                aspect: [4,3]
            });
            if (!capturedImage.cancelled) {
                this.processImage(capturedImage.uri);
            }
        }
    }

    getImageFromGallery = async() => {
        const galleryPermission = await Permissions.askAsync(Permissions.CAMERA_ROLL);
        if (galleryPermission.status === 'granted') {
            let chosedImage = await ImagePicker.launchImageLibraryAsync({
                allowsEditing: true,
                aspect: [4, 3]
            });
            if (!chosedImage.cancelled) {
                this.processImage(chosedImage.uri);
            }
        }
    }

    processImage = async (imageUri) => {
        let processedImage = await ImageManipulator.manipulateAsync(
            imageUri,
            [   
                {resize: {width: 400}}
            ], 
            {format: 'png'}
        );
        this.setState({imageUrl: processedImage.uri})
    }
    handleRegister() {
        console.log(JSON.stringify(this.state));
        if(this.state.remember) {
            SecureStore.setItemAsync('userinfo', JSON.stringify({username: this.state.username, password: this.state.password}))
                .catch(error => console.log("Couldn't save data, ", error))
        }
    }
    static navigationOptions = {
        title: 'Register',
        tabBarIcon: ({tintColor}) => (
            <Icon 
                name='user-plus'
                type='font-awesome'
                size={24}
                iconStyle={{color: tintColor}}
            />
        )
    };

    render() {
        return(
            <ScrollView>
            <View style={styles.container}>
                <View style={styles.imageContainerStyle}>
                    <Image 
                        source={{uri: this.state.imageUrl}}
                        loadingIndicatorSource={require('../shared/images/logo.png')}
                        style={styles.image} />
                    <Button 
                        title="Camera"
                        onPress={this.getImageFromCamera}
                    />
                    <Button 
                        title="Gallery"
                        onPress={this.getImageFromGallery}
                    />
                </View>
                <Input 
                    placeholder="Username"
                    leftIcon={{type:'font-awesome', name:'user-o'}}
                    onChangeText={(username) => this.setState({username})}
                    value={this.state.username}
                    containerStyle={styles.formInput}
                />
                <Input 
                    placeholder="Password"
                    leftIcon={{type:'font-awesome', name:'key'}}
                    onChangeText={(password) => this.setState({password})}
                    value={this.state.password}
                    containerStyle={styles.formInput}
                />
                <Input 
                    placeholder="Firstname"
                    leftIcon={{type:'font-awesome', name:'user-o'}}
                    onChangeText={(firstname) => this.setState({firstname})}
                    value={this.state.firstname}
                    containerStyle={styles.formInput}
                />
                <Input 
                    placeholder="Lastname"
                    leftIcon={{type:'font-awesome', name:'user-o'}}
                    onChangeText={(lastname) => this.setState({lastname})}
                    value={this.state.lastname}
                    containerStyle={styles.formInput}
                />
                <Input 
                    placeholder="Email"
                    leftIcon={{type:'font-awesome', name:'envelope-o'}}
                    onChangeText={(email) => this.setState({email})}
                    value={this.state.email}
                    containerStyle={styles.formInput}
                />
                <CheckBox 
                    title="Remeber Me"
                    center
                    checked={this.state.remember}
                    onPress={() => this.setState({remember: !this.state.remember})}
                    containerStyle={styles.formCheckBox}
                />
                <View style={styles.formButton}>
                    <Button 
                        onPress={() => this.handleRegister()}
                        title="Register"
                        buttonStyle={{backgroundColor: "#512DA8"}}
                        icon={<Icon name='user-plus' size={24} type='font-awesome' color='white' />}
                    />
                </View>
            </View>
            </ScrollView>
        )
    }
}

const Login = createBottomTabNavigator({
    Login: LoginTabComponent,
    Register: RegisterTabComponent,
}, {
    tabBarOptions: {
        activeBackgroundColor: '#9575CD',
        inactiveBackgroundColor: '#D1C4E9',
        activeTintColor: 'white',
        inactiveTintColor: 'gray'
    }
})

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        margin: 20,
    },
    formInput: {
        marginVertical: 10
    },
    formCheckBox: {
        margin: 40,
        backgroundColor: null
    },
    formButton: {
        margin: 60
    },
    imageContainerStyle: {
        flex: 1,
        flexDirection: 'row',
        margin: 20,
        justifyContent: 'space-around'
    },
    image: {
        margin: 10,
        width: 80,
        height: 60
    }
})
export default Login;